import React from "react";
import ModalWrapper from "./ModalWrapper.jsx";
import ModalBox from "./ModalBox.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalFooter from "./ModalFooter.jsx";
import "./ModalBase.scss";

const ModalBase = () => {
    return (
        <ModalWrapper>
            <ModalBox>
                <ModalClose />
                <ModalHeader>
                    Modal Header
                </ModalHeader>
                <ModalBody>
                    Modal Body
                </ModalBody>
                <ModalFooter>
                    Modal Footer
                </ModalFooter>
            </ModalBox>
        </ModalWrapper>
    )
};

export default ModalBase;


// import PropTypes from "prop-types";
// import "./Modal.scss";

// const ModalBox = (props) => {
//     const {
//         className,
//         children,
//         ...restProps
//     } = props;

//     return (
//         <div
//             className={cn("modal", className)}

//             {...restProps}
//         >
//             {children}
//         </div >
//     )
// };

// Modal.propTypes = {
//     className: PropTypes.string,
//     children: PropTypes.any,
//     restProps: PropTypes.object
// };