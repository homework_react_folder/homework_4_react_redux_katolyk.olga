// конфігуруємо наш store для підключення slice.
import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./slices/slice.js"; // nameSlice1, nameSlice2, nameSlice3, якщо ред'юсерів декілька, декілька сутностей
import localStorageMiddleware from "./middleware/localStorageMiddleware.js";

const loadState = () => {
    try {
        const serializedBasketList = localStorage.getItem('basketList');
        const serializedProducts = localStorage.getItem('mainProductList');
        const serializedFavoritedList = localStorage.getItem('favoritedList');

        return {
            basketList: serializedBasketList ? JSON.parse(serializedBasketList) : [],
            products: serializedProducts ? JSON.parse(serializedProducts) : [],
            favoritedList: serializedFavoritedList ? JSON.parse(serializedFavoritedList) : [],
        };
    } catch (err) {
        return undefined;
    }
};

const preloadedState = loadState();

const store = configureStore({
    reducer: rootReducer,
    preloadedState,  // Додаємо стан, завантажений з Local Storage
    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware().concat(localStorageMiddleware),
});

export default store;








// export default configureStore({
//     reducer: rootReducer,
//     middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk,logger)
// })


// import nameSlice1 from "./slices/slice1.js"; // и в таком случаии эти файлы можно размещать где угодно
// import nameSlice2 from "./slices/slice2.js"; // и в таком случаии эти файлы можно размещать где угодно
// import nameSlice3 from "./slices/slice3.js"; // и в таком случаии эти файлы можно размещать где угодно

// export default configureStore({
// 	reducer: {
// 		slice1: nameSlice1,
// 		slice2: nameSlice2,
// 		slice3: nameSlice3,
// 	},
// })