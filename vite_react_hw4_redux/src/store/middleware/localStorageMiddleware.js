// Middleware у Redux - це функції, які стоять між дією (action) і ред'юсером (reducer). 
// Вони дозволяють перехоплювати або змінювати дії перед тим, як вони дійдуть до ред'юсера. 
// Це корисно для таких речей, як логування, обробка асинхронних запитів, та інші побічні ефекти.

// Як працює Middleware?
// Коли ви диспатчите (викликаєте) дію, вона спочатку проходить через всі підключені middleware, 
// перш ніж досягне ред'юсера. 

// Кожен middleware має можливість:
// - Перехопити дію і зробити з нею щось (наприклад, логувати її).
// - Змінити дію.
// - Відмовитися від дії (пропустити її).
// - Продовжити дію до наступного middleware або ред'юсера.
// Чи потрібна додаткова бібліотека?
// Redux поставляється з основними middleware (як redux-thunk для обробки асинхронних дій), 
// але створення власного middleware не потребує додаткових бібліотек.
// Middleware дозволяє вам впроваджувати логіку, яка працює з діями перед тим, 
// як вони досягнуть ред'юсера. 

const localStorageMiddleware = store => next => action => {
    // Викликаємо наступний middleware або ред'юсер, 
    // додаємо умови для збереження стану в localStorage при будь-якій зміні в масивах
    const result = next(action);
    const state = store.getState();

    switch (action.type) {
        case "root/actionAddToBasket":
        case "root/actionRemoveFromBasket":
        case "root/actionAddToFavorited":
        case "root/actionUpdateProducts":
        case "root/actionSyncProductsWithFavoritedAndBasket":
        case "root/actionSyncFavoritedWithBasket":
        case "root/actionSyncBasketWithFavorited":
            localStorage.setItem("basketList", JSON.stringify(state.basketList));
            localStorage.setItem("mainProductList", JSON.stringify(state.products));
            localStorage.setItem("favoritedList", JSON.stringify(state.favoritedList));
            break;
        default:
            break;
    }

    return result;
};

export default localStorageMiddleware;

// Другий спосіб, з використання типів actions:
// const localStorageMiddleware = store => next => action => {
//     // Викликаємо наступний middleware або ред'юсер, додаємо умови для збереження стану при будь-якій зміні в масивах
//     const result = next(action);
    
//     // Слідкуємо за діями, які змінюють basketList, favoritedList і products, перевіряємо типи дій, які нас цікавлять
//     if (action.type === 'root/actionAddToBasket' || action.type === 'root/actionRemoveFromBasket') {
//         const basketList = store.getState().basketList;
//         localStorage.setItem('basketList', JSON.stringify(basketList));

//         const products = store.getState().products;
//         localStorage.setItem('mainProductList', JSON.stringify(products));
//     }

//     if (action.type === 'root/actionFetchProducts/fulfilled' || action.type === 'root/actionUpdateProducts') {
//         const products = store.getState().products;
//         localStorage.setItem('mainProductList', JSON.stringify(products));
//     }

//     if (action.type === 'root/actionAddToFavorited') {
//         const favoritedList = store.getState().favoritedList;
//         localStorage.setItem('favoritedList', JSON.stringify(favoritedList));

//         const products = store.getState().products;
//         localStorage.setItem('mainProductList', JSON.stringify(products));
//     }

//     return result;
// };

// export default localStorageMiddleware;