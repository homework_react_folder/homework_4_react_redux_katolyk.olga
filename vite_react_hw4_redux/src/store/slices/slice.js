import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
	products: [],
    isAddBasketModal: false,
    modalContent: {
        imgCard: undefined,
        modalText: "Add to basket",
        nameCard: undefined,
        price: undefined,
        article: undefined,
        color: undefined,
        isFavorited: undefined,
        countInBasket: undefined,
    },
    favoritedList: [],
    basketList: [],    
};

export const actionFetchProducts = createAsyncThunk(
    "products/fetchProducts",
    async () => {
        const response = await fetch("../../../public/products.json");
        const data = await response.json();
        return data;
    }
);

const rootSlice = createSlice({
    name: "root",  // назва нашого ред'юсера
    initialState,  // інішнл стейт це базові значення для нашого додатка, коротка форма запису "ключ: значення," коли ключ і значення - однакові

    // reducers у createSlice використовуються для визначення функцій, які змінюють стан (state) у вашому Redux store. 
    // Кожна функція в reducers описує, як стан повинен змінюватися у відповідь на певну дію (action).     
    reducers: {
        actionToggleAddBasketModal: (state) => {
            state.isAddBasketModal = !state.isAddBasketModal;
        },

        actionSetModalContent: (state, action) => {
            state.modalContent = action.payload;
        },

        actionSetModalText: (state, action) => { // дія для зміни modalText на кнопці
            state.modalContent.modalText = action.payload;
        },

        actionAddToFavorited: (state, action) => {
            const item = action.payload;

            const existingItem = state.favoritedList.find(product => product.article === item.article);
            if(existingItem) {
                existingItem.isFavorited = !existingItem.isFavorited;
                state.favoritedList = state.favoritedList.filter(product => product.article !== item.article);
            } else {
                state.favoritedList.push({...item, isFavorited: true});
            }            
        },

        actionAddToBasket: (state, action) => {
            const item = action.payload;
            const existingItem = state.basketList.find(product => product.article === item.article);
            if(existingItem) {
                existingItem.countInBasket += 1;
            } else {
                state.basketList.push({...item, countInBasket: 1});
            }             
        },

        actionRemoveFromBasket: (state, action) => {
            const item = action.payload;
            state.basketList = state.basketList.filter(product => product.article !== item.article);
        },

        actionUpdateProducts: (state, action) => {
            state.products = action.payload;
        },
        actionSyncProductsWithFavoritedAndBasket: (state) => {
            state.products.forEach(product => {
              const basketItem = state.basketList.find(item => item.article === product.article);
              const favoritedItem = state.favoritedList.find(item => item.article === product.article);
              if (basketItem) {
                product.countInBasket = basketItem.countInBasket;
              } else {
                product.countInBasket = 0;
              }
              product.isFavorited = favoritedItem ? true : false;
            });
          },
        actionSyncFavoritedWithBasket: (state) => {
            state.favoritedList.forEach(favorite => {
                const basketItem = state.basketList.find(item => item.article === favorite.article);
                favorite.countInBasket = basketItem ? basketItem.countInBasket : 0;
            });
        },
        actionSyncBasketWithFavorited: (state) => {
            state.basketList.forEach(basket => {
                const favoritedItem = state.favoritedList.find(item => item.article === basket.article);
                basket.isFavorited = favoritedItem ? true : false;
            });
        },
    },
    extraReducers: (builder) => {
            // builder.addCase(actionFetchProducts.fulfilled, (state, { payload }) => {
            //     state.products = payload;
            // })    
            builder.addCase(actionFetchProducts.fulfilled, (state, action) => {
                    state.products = action.payload;
                })                
        }    
});

// selectors — це функції, які використовуються для витягання даних зі стану (state). 
// Вони не змінюють стан, а тільки отримують певну інформацію з нього, тому ми їх НЕ вставляємо в reducers
export const selectTotalBasketItems = (state) => {
    return state.basketList.reduce((total, item) => total + item.countInBasket, 0);
  }; 

export const { 
    actionToggleAddBasketModal, 
    actionSetModalContent, 
    actionSetModalText, 
    actionAddToFavorited,
    actionAddToBasket,
    actionUpdateProducts,
    actionRemoveFromBasket,
    actionSyncProductsWithFavoritedAndBasket,
    actionSyncFavoritedWithBasket,
    actionSyncBasketWithFavorited,
} = rootSlice.actions;

export default rootSlice.reducer; // і потім генеруємо сам редюсер, який підключаємо в нашем configureStore

// const index = state.favoritedList.findIndex(product => product.article === item.article);
            // if (index !== -1) {
            //     state.favoritedList.splice(index, 1);
            // } else {
            //     state.favoritedList.push({ ...item, isFavorited: true });
            // }
            // const existingItemInProducts = state.products.find(product => product.article === item.article);
            // if (existingItemInProducts) {
            //     existingItemInProducts.isFavorited = !existingItemInProducts.isFavorited;
            // }
            // const existingItemInBasket = state.basketList.find(product => product.article === item.article);
            // if (existingItemInBasket) {
            //     existingItemInBasket.isFavorited = !existingItemInBasket.isFavorited;
            // }
// const index = state.basketList.findIndex(product => product.article === item.article);
            // if (index !== -1) {
            //     state.basketList[index].countInBasket += 1;
            // } else {
            //     state.basketList.push({ ...item, countInBasket: 1 });
            // }       
            // const existingItemInProducts = state.products.find(product => product.article === item.article);
            // if (existingItemInProducts) {
            //     existingItemInProducts.countInBasket = (existingItemInProducts.countInBasket || 0) + 1;
            // }            
            // const existingItemInFavorited = state.favoritedList.find(product => product.article === item.article);
            // if (existingItemInFavorited) {
            //     existingItemInFavorited.countInBasket = (existingItemInFavorited.countInBasket || 0) + 1;
            // }          





// export const { actionProducts, actionModalContent, actionFavoritedList, actionBasketList } = rootSlice.actions // коли завершили написання ред'юсерів, генеруємо actions
 // об'єкт (назва в множині) наших ред'юсерів, кожний метод в ньому з 2 частей: 
        // ліва - назва екшена, функції для виконання, 
        // права - код функції, яка повинна обновити наш глобальний стейт
        // actionProducts: (state, action) => {},

        // actionModalContent: (state, action) => {},

        // actionFavoritedList: (state, action) => {},

        // actionBasketList: (state, action) => {},

 // actionRemoveFromBasket: (state, action) => {
        //     const item = action.payload;
        //     const indexInBasket = state.basketList.findIndex(product => product.article === item.article);
        //     if (indexInBasket !== -1) {
        //         if (state.basketList[indexInBasket].countInBasket > 1) {
        //             state.basketList[indexInBasket].countInBasket -= 1;
        //         } else {
        //             state.basketList.splice(indexInBasket, 1);
        //         }
        //     }
        //     const existingItemInProducts = state.products.find(product => product.article === item.article);
        //     if (existingItemInProducts) {
        //         existingItemInProducts.countInBasket = (existingItemInProducts.countInBasket || 0) - 1;
        //     }
        // },   
        
        // if (!Array.isArray(state.favoritedList)) {
        //     state.favoritedList = [];
// }